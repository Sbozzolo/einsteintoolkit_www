#!/usr/bin/python3
from kuibit.simdir import SimDir
import matplotlib.pyplot as plt
PATH="tov_gallery"
rho = SimDir(PATH).timeseries.maximum['rho']
plt.plot(rho / rho(0))
plt.xlabel(r'$t[M_{sun}]$')
plt.ylabel(r'$\rho_c/\rho_c(0)$')
plt.tick_params(axis='both',direction='in',top=True,right=True)
plt.savefig("density.pdf")
plt.savefig("density.png")
